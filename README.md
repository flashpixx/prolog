# Prolog Service

* [http://localhost:8080/hello_world](http://localhost:8080/hello_world)
* [http://localhost:8080/database](http://localhost:8080/database)
* PGAdmin [http://localhost:9090](http://localhost:9090) (`root@no.com` / `root`)
* Postgres Port `5432` Password `root`

## Tools

### Prolog

* https://hub.docker.com/_/swipl
* https://www.swi-prolog.org/howto/http/
* https://www.swi-prolog.org/pldoc/man?section=odbc-query
* https://swi-prolog.discourse.group/t/swi-prolog-connecting-to-postgresql-via-odbc/2404

### Inference Database

* https://vaticle.com/typedb
* https://docs.vaticle.com/docs/running-typedb/install-and-run
* https://github.com/vaticle/typedb-studio
* https://docs.vaticle.com/docs/console/console
    * `dockerexec vaticle bash -c '/opt/typedb-all-linux/typedb console'`
    * `database create data`
