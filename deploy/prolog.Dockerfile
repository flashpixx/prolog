FROM swipl:latest

RUN apt-get update && \
    apt-get --yes --no-install-recommends install unixodbc odbc-postgresql swi-prolog-odbc && \
    rm -rf /var/lib/apt/lists/*

EXPOSE 8080

CMD ["swipl", "/app/start.pl"]
