:- use_module(library(odbc)).
:- use_module(library(http/thread_httpd)).
:- use_module(library(http/http_dispatch)).

:- http_handler(root(hello_world), say_hi, []).
:- http_handler(root(database), query, []).


server(Port) :-
    http_server(http_dispatch, [port(Port)]).

say_hi(_Request) :-
    format('Content-type: text/plain~n~n'),
    format('Hello World!~n').

query(_Request) :-
    odbc_connect('Postgres', Connection, []),
    odbc_query(Connection, 'SELECT COUNT(*) FROM data',COUNT),
    format('Content-type: text/plain~n~n'),
    format('row number: ~w~n',[COUNT]),
    odbc_disconnect(Connection).


:- server(8080).