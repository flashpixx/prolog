CREATE SEQUENCE IF NOT EXISTS public.data_id_seq
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;

CREATE TABLE IF NOT EXISTS public.data
(
    id integer NOT NULL DEFAULT nextval('data_id_seq'::regclass),
    value character varying COLLATE pg_catalog."default" NOT NULL,
    CONSTRAINT data_pkey PRIMARY KEY (id)
);

INSERT INTO public.data(value) VALUES ('hello');
INSERT INTO public.data(value) VALUES ('foobar');